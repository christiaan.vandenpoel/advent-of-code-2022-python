from aoc.day12 import part1, part2

#
# --- Part One ---
#

first_example_input = """Sabqponm
abcryxxl
accszExk
acctuvwj
abdefghi""".splitlines()

def test_part1():
    assert part1.result(first_example_input) == 31

#
# --- Part Two ---
#

def test_part2():
    assert part2.result(first_example_input) == 29
