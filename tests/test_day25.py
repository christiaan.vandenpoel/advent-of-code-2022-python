from aoc.day25 import part1, part2, snafu
import pytest

#
# --- Part One ---
#

snafu_examples = [
    ('1', 1),
    ('2', 2),
    ('1=', 3),
    ('1-', 4),
    ('10', 5),
    ('11', 6),
    ('12', 7),
    ('2=', 8),
    ('2-', 9),
    ('20', 10),
    ('21', 11),
    ('22', 12),
    ('1==', 13),
    ('1=-', 14),
    ('1=0', 15),
    ('1=1', 16),
    ('1=2', 17),
    ('1-=', 18),
    ('1--', 19),
    ('1-0', 20),
    ('1-1', 21),
    ('1-2', 22),
    ('10=', 23),
    ('10-', 24),
    ('100', 25),
    ('1=11-2', 2022),
    ('1-0---0', 12345),
    ('1121-1110-1=0', 314159265),
    ('1=-0-2', 1747),
    ('12111', 906),
    ('2=0=', 198),
    ('21',  11),
    ('2=01', 201),

    ('111',  31),
    ('20012', 1257),
    ('112',  32),
    ('1=-1=', 353),
    ('1-12', 107),
    ('12',   7),
    ('1=',   3),
    ('122',  37),
]

@pytest.mark.parametrize("snafu_ex,decimal", snafu_examples)
def test_from_snafu(snafu_ex,decimal):
    assert snafu.from_snafu(snafu_ex) == decimal

@pytest.mark.parametrize("snafu_ex,decimal", snafu_examples)
def test_to_snafu(snafu_ex,decimal):
    assert snafu.to_snafu(decimal) == snafu_ex


example = """1=-0-2
12111
2=0=
21
2=01
111
20012
112
1=-1=
1-12
12
1=
122""".splitlines()

# @pytest.mark.skip
def test_part1():
    assert part1.result(example) == "2=-1=0"