from aoc.day22 import part1, part2
import pytest

#
# --- Part One ---
#

example = """        ...#
        .#..
        #...
        ....
...#.......#
........#...
..#....#....
..........#.
        ...#....
        .....#..
        .#......
        ......#.

10R5L5R10L4R5L5""".splitlines()

def test_part1():
    assert part1.result(example) == 6032

#
# --- Part Two ---
#
def off_board_func(position, size, new_x, new_y, direction, board):
    x,y = position

    # check if we drop of the board
    if board[(new_x, new_y)] == ' ':
        print("Dropping of the board - from: {0} to {1} ({2})".format((x,y), (new_x, new_y), direction))
        if x == 11 and y in range(4, 8) and direction == ">":
            direction = "v"
            new_x = 15 - (y % size)
            new_y = 8
        elif y == 11 and x in range(8, 12) and direction == 'v':
            direction = "^"
            new_x = 3 - ( x % size)
            new_y = 7
        elif y == 4 and x in range(4, 8) and direction == "^":
            direction = ">"
            new_x = 8
            new_y = 0 + x % size
        else:
            raise NotImplementedError("unsupported {0} > {1} '{2}'".format((x,y), (new_x, new_y), direction))

    return new_x, new_y, direction


# @pytest.mark.skip
def test_part2():
    assert part2.result(example, off_board_func) == 5031
