from aoc.day20 import part1, part2
import pytest

#
# --- Part One ---
#
example = """1
2
-3
3
-2
0
4""".splitlines()

# @pytest.mark.skip
def test_part1():
    assert part1.result(example) == 3

#
# --- Part Two ---
#

# @pytest.mark.skip
def test_part2():
    assert part2.result(example) == 1623178306
