from aoc.day03 import part1, part2

first_test_input = """vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw""".splitlines()

#
# --- Part One ---
#

def test_part1():
    assert part1.result(first_test_input) == 157

#
# --- Part Two ---
#

def test_part2():
    assert part2.result(first_test_input) == 70
