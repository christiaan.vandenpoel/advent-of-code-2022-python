from aoc.day17 import part1, part2
import pytest

#
# --- Part One ---
#
example = """>>><<><>><<<>><>>><<<>>><<<><<<>><>><<>>""".splitlines()

@pytest.mark.skip
def test_part1():
    assert part1.result(example, total_rocks=2022) == 3068

#
# --- Part Two ---
#

# @pytest.mark.skip
def test_part2():
    total_rocks = 1_000_000_000_000
    assert part1.result(example, total_rocks=total_rocks) == 1514285714288
