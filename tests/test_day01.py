from aoc.day01 import part1, part2

#
# --- Part One ---
#

first_test_example = """1000
2000
3000

4000

5000
6000

7000
8000
9000

10000""".splitlines()

def test_part1():
    assert part1.result(first_test_example) == 24000

#
# --- Part Two ---
#

def test_part2():
    assert part2.result(first_test_example) == 45000
