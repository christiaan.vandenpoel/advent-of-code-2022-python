from aoc.day06 import part1, part2

first_test_examples = {
    'mjqjpqmgbljsphdztnvjfqwrcgsmlb': 7,
    'bvwbjplbgvbhsrlpgdmjqwftvncz': 5,
    'nppdvjthqldpwncqszvftbrmjlhg': 6,
    'nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg': 10,
    'zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw': 11
}

second_test_examples = {
    'mjqjpqmgbljsphdztnvjfqwrcgsmlb': 19,
    'bvwbjplbgvbhsrlpgdmjqwftvncz': 23,
    'nppdvjthqldpwncqszvftbrmjlhg': 23,
    'nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg': 29,
    'zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw': 26
}

#
# --- Part One ---
#

def test_part1():
    for test, result in first_test_examples.items():
        assert part1.result([test]) == result

#
# --- Part Two ---
#

def test_part2():
    for test, result in second_test_examples.items():
        assert part2.result([test]) == result
