from aoc.day09 import part1, part2

first_test_example = """R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2""".splitlines()

second_test_example = """R 5
U 8
L 8
D 3
R 17
D 10
L 25
U 20""".splitlines()

#
# --- Part One ---
#

def test_part1():
    assert part1.result(first_test_example) == 13

#
# --- Part Two ---
#

def test_part2():
    assert part2.result(first_test_example) == 1
    print("boo")
    assert part2.result(second_test_example) == 36

def test_movements():
    rope_bridge = part1.RopeBridge()
    
    # single steps - every direction
    rope_bridge.move("R 1".splitlines())
    assert rope_bridge.head() == (1,0)
    assert rope_bridge.tail() == (0,0)
    assert len(rope_bridge.tail_trail()) == 1

    rope_bridge = part1.RopeBridge()
    rope_bridge.move("U 1".splitlines())
    assert rope_bridge.head() == (0,1)
    assert rope_bridge.tail() == (0,0)
    assert len(rope_bridge.tail_trail()) == 1

    rope_bridge = part1.RopeBridge()
    rope_bridge.move("L 1".splitlines())
    assert rope_bridge.head() == (-1,0)
    assert rope_bridge.tail() == (0,0)
    assert len(rope_bridge.tail_trail()) == 1

    rope_bridge = part1.RopeBridge()
    rope_bridge.move("D 1".splitlines())
    assert rope_bridge.head() == (0,-1)
    assert rope_bridge.tail() == (0,0)
    assert len(rope_bridge.tail_trail()) == 1

    # longer steps
    rope_bridge = part1.RopeBridge()
    rope_bridge.move(["R 2"])
    assert rope_bridge.head() == (2,0)
    assert rope_bridge.tail() == (1,0)
    assert len(rope_bridge.tail_trail()) == 2

    rope_bridge = part1.RopeBridge()
    rope_bridge.move(["L 2"])
    assert rope_bridge.head() == (-2,0)
    assert rope_bridge.tail() == (-1,0)
    assert len(rope_bridge.tail_trail()) == 2

    rope_bridge = part1.RopeBridge()
    rope_bridge.move(["U 2"])
    assert rope_bridge.head() == (0,2)
    assert rope_bridge.tail() == (0,1)
    assert len(rope_bridge.tail_trail()) == 2

    rope_bridge = part1.RopeBridge()
    rope_bridge.move(["D 2"])
    assert rope_bridge.head() == (0,-2)
    assert rope_bridge.tail() == (0,-1)
    assert len(rope_bridge.tail_trail()) == 2

    rope_bridge = part1.RopeBridge()
    rope_bridge.move(["D 3"])
    assert rope_bridge.head() == (0,-3)
    assert rope_bridge.tail() == (0,-2)
    assert len(rope_bridge.tail_trail()) == 3

    # multiple different steps
    rope_bridge = part1.RopeBridge()
    rope_bridge.move(["U 1","R 1"])
    assert rope_bridge.head() == (1,1)
    assert rope_bridge.tail() == (0,0)
    assert len(rope_bridge.tail_trail()) == 1

    rope_bridge = part1.RopeBridge()
    rope_bridge.move(["U 1","L 1"])
    assert rope_bridge.head() == (-1,1)
    assert rope_bridge.tail() == (0,0)
    assert len(rope_bridge.tail_trail()) == 1

    rope_bridge = part1.RopeBridge()
    rope_bridge.move(["R 1","U 1"])
    assert rope_bridge.head() == (1,1)
    assert rope_bridge.tail() == (0,0)
    assert len(rope_bridge.tail_trail()) == 1

    rope_bridge = part1.RopeBridge()
    rope_bridge.move(["R 1","D 1"])
    assert rope_bridge.head() == (1,-1)
    assert rope_bridge.tail() == (0,0)
    assert len(rope_bridge.tail_trail()) == 1

    rope_bridge = part1.RopeBridge()
    rope_bridge.move(["R 1","L 1"])
    assert rope_bridge.head() == (0,0)
    assert rope_bridge.tail() == (0,0)
    assert len(rope_bridge.tail_trail()) == 1

    rope_bridge = part1.RopeBridge()
    rope_bridge.move(["U 1","R 2"])
    assert rope_bridge.head() == (2,1)
    assert rope_bridge.tail() == (1,1)
    assert len(rope_bridge.tail_trail()) == 2

    rope_bridge = part1.RopeBridge()
    rope_bridge.move(["L 1","D 2"])
    assert rope_bridge.head() == (-1,-2)
    assert rope_bridge.tail() == (-1,-1)
    assert len(rope_bridge.tail_trail()) == 2