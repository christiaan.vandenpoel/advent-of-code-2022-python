from aoc.day08 import part1, part2

first_test_example = """30373
25512
65332
33549
35390""".splitlines()

#
# --- Part One ---
#

def test_part1():
    assert part1.result(first_test_example) == 21

#
# --- Part Two ---
#

def test_part2():
    assert part2.result(first_test_example) == 8
