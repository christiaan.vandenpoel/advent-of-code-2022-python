from aoc.day24 import part1, part2
import pytest

#
# --- Part One ---
#
example = """#.######
#>>.<^<#
#.<..<<#
#>v.><>#
#<^v^^>#
######.#""".splitlines()

# @pytest.mark.skip
def test_part1():
    assert part1.result(example) == 18

#
# --- Part Two ---
#

# @pytest.mark.skip
def test_part2():
    assert part2.result(example) == 54
