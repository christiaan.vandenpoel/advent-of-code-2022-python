from aoc.day19 import part1, part2
import pytest

#
# --- Part One ---
#
example = """Blueprint 1: Each ore robot costs 4 ore. Each clay robot costs 2 ore. Each obsidian robot costs 3 ore and 14 clay. Each geode robot costs 2 ore and 7 obsidian.
Blueprint 2: Each ore robot costs 2 ore. Each clay robot costs 3 ore. Each obsidian robot costs 3 ore and 8 clay. Each geode robot costs 3 ore and 12 obsidian.""".splitlines()

# @pytest.mark.skip
def test_part1():
    assert part1.result(example, minutes = 24) == 33

#
# --- Part Two ---
#

@pytest.mark.skip
def test_part2():
    assert part2.result(None) == None
