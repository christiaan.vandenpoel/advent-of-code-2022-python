from aoc.day13 import part1, part2
from aoc.day13.distress_signal import is_in_right_order

#
# --- Part One ---
#
first_test = """[1,1,3,1,1]
[1,1,5,1,1]

[[1],[2,3,4]]
[[1],4]

[9]
[[8,7,6]]

[[4,4],4,4]
[[4,4],4,4,4]

[7,7,7,7]
[7,7,7]

[]
[3]

[[[]]]
[[]]

[1,[2,[3,[4,[5,6,7]]]],8,9]
[1,[2,[3,[4,[5,6,0]]]],8,9]""".splitlines()

def test_is_in_right_order_simple_lists():
    assert is_in_right_order([1,1,3,1,1],[1,1,5,1,1]) == True

def test_is_in_right_order_inner_list_vs_single_int():
    assert is_in_right_order([[1],[2,3,4]],[[1],4]) == True

def test_is_in_right_order_inner_list_vs_double_inner_list():
    assert is_in_right_order([9], [[8,7,6]]) == False

def test_is_in_right_order_first_is_shorter_than_second():
    assert is_in_right_order([[4,4],4,4],[[4,4],4,4,4]) == True

def test_is_in_right_order_first_is_longer_than_second():
    assert is_in_right_order([7,7,7,7],[7,7,7]) == False

def test_is_in_right_order_first_is_empty():
    assert is_in_right_order([],[3]) == True

def test_is_in_right_order_first_more_nested_empty_list_than_second():
    assert is_in_right_order([[[]]], [[]]) == False

def test_is_in_right_order_many_nested_lists():
    assert is_in_right_order([1,[2,[3,[4,[5,6,7]]]],8,9], [1,[2,[3,[4,[5,6,0]]]],8,9]) == False

def test_is_in_right_order_many_nested_lists_2():
    assert is_in_right_order([1,[2,[3,[4,[5,6,7]]]],10,9], [1,[2,[3,[4,[5,6,7]]]],8,9]) == False

def test_is_in_right_order_real_input_1():
    first = [[[10], 7, 3, [[6, 4, 4, 1], [], [2, 6, 7, 10]], 6], []]
    second =  [[2, [3, 5, [8, 6, 7, 1, 9], [6]]], [[4, 2, 10], [], [[8, 5, 5, 9], [7], 5], [], 9]]
    assert is_in_right_order(first, second) == False


def test_part1():
    assert part1.result(first_test) == 13

#
# --- Part Two ---
#

def test_part2():
    assert part2.result(first_test) == 140
