from aoc.day18 import part1, part2
import pytest

#
# --- Part One ---
#

first_example = """2,2,2
1,2,2
3,2,2
2,1,2
2,3,2
2,2,1
2,2,3
2,2,4
2,2,6
1,2,5
3,2,5
2,1,5
2,3,5""".splitlines()

def test_part1():
    assert part1.result(first_example) == 64

#
# --- Part Two ---
#

# @pytest.mark.skip
def test_part2():
    assert part2.result(first_example) == 58
