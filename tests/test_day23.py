from aoc.day23 import part1, part2
import pytest

#
# --- Part One ---
#

# answer is 25
first_example = """.....
..##.
..#..
.....
..##.
.....""".splitlines()

# answer is 110
second_example = """....#..
..###.#
#...#.#
.#...##
#.###..
##.#.##
.#..#..""".splitlines()

def test_part1():
    assert part1.result(first_example, rounds=10) == 25
    assert part1.result(second_example, rounds=10) == 110

#
# --- Part Two ---
#

def test_part2():
    assert part2.result(second_example) == 20
