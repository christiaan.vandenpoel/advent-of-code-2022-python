This repository is a collection of my solutions for the [Advent of Code 2022](http://adventofcode.com/2022) calendar.

Puzzle                                                    |
--------------------------------------------------------- |
[Day 1: ⭐️⭐️ Calorie Counting](aoc/day01)                  |  
[Day 2: ⭐️⭐️ Rock Paper Scissors](aoc/day02)               |
[Day 3: ⭐️⭐️ Rucksack Reorganization](aoc/day03)           |
[Day 4: ⭐️⭐️ Camp Cleanup](aoc/day04)                      |
[Day 5: ⭐️⭐️ Supply Stacks](aoc/day05)                     |
[Day 6: ⭐️⭐️ Tuning Trouble](aoc/day06)                    |
[Day 7: ⭐️⭐️ No Space Left On Device](aoc/day07)           |   
[Day 8: ⭐️⭐️ Treetop Tree House](aoc/day08)                |
[Day 9: ⭐️⭐️ Rope Bridge](aoc/day09)                       |
[Day 10: ⭐️⭐️ Cathode-Ray Tube](aoc/day10)                 |
[Day 11: ⭐️⭐️ Monkey in the Middle](aoc/day11)             |
[Day 12: ⭐️⭐️ Hill Climbing Algorithm](aoc/day12)          |
[Day 13: ⭐️⭐️ Distress Signal](aoc/day13)                  |
[Day 14: ⭐️⭐️ Regolith Reservoir](aoc/day14)               |
[Day 15: ⭐️⭐️ Beacon Exclusion Zone](aoc/day15)            |
[Day 16: ⭐️⭐️ Proboscidea Volcanium](aoc/day16)            |
[Day 17: ⭐️⭐️ Pyroclastic Flow](aoc/day17)                 |
[Day 18: ⭐️⭐️ Boiling Boulders](aoc/day18)                 |
[Day 19: ⭐️⭐️ Not Enough Minerals](aoc/day19)              |
[Day 20: ⭐️⭐️ Grove Positioning System](aoc/day20)         |
[Day 21: ⭐️⭐️ Monkey Math](aoc/day21)                      |
[Day 22: ⭐️⭐️ Monkey Map](aoc/day22)                       |
[Day 23: ⭐️⭐️ Unstable Diffusion](aoc/day23)               |
[Day 24: ⭐️⭐️ Blizzard Basin](aoc/day24)                   |
[Day 25: ⭐️⭐️ Full of Hot Air](aoc/day25)                  |
