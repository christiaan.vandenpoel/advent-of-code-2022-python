# Advent of Code - Day 25 - Part One
from .snafu import from_snafu, to_snafu

def result(input):
    return to_snafu(sum([from_snafu(l) for l in input]))
