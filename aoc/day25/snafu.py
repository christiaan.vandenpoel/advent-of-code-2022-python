from collections import defaultdict
def from_snafu(snafu):
    mapping ={
        '=': -2,
        '-': -1,
        '0': 0,
        '1': 1,
        '2': 2,
    }

    decimal = 0
    for order, char in enumerate(reversed(snafu)):
        decimal += 5**order * mapping[char]

    return decimal

def to_snafu(decimal):
    exponent_map = defaultdict(int)

    exponent = 0
    while decimal > 0:
        rem = decimal % 5
        exponent_map[exponent] += rem
        
        exponent += 1
        decimal //= 5

    for exp in sorted(exponent_map.keys()):
        if exponent_map[exp] == 3:
            exponent_map[exp+1] += 1
            exponent_map[exp] = -2
        elif exponent_map[exp] == 4:
            exponent_map[exp+1] += 1
            exponent_map[exp] = -1
        elif exponent_map[exp] == 5:
            exponent_map[exp+1] += 1
            exponent_map[exp] = 0
        elif exponent_map[exp] in [0,1,2]:
            pass
        else:
            raise NotImplementedError(f"unsupported value {exponent_map[exp]} for exp={exp}")

    result = ''
    for exp in sorted(exponent_map.keys()):
        val = exponent_map[exp]
        if val in [0,1,2]:
            result += str(val)
        elif val == -1:
            result += '-'
        elif val == -2:
            result += '='
        else:
            raise NotImplementedError(f"unsupported value {val}")

    return "".join(reversed(result))