# Advent of Code - Day 1 - Part Two

def result(input):
    result = list([0])
    for calory in input:
        if calory == '':
            result.append(0)
        else:
            result[-1] += int(calory)
    return sum(sorted(result, reverse=True)[0:3])