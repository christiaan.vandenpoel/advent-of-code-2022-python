# Advent of Code - Day 1 - Part One

def result(input):
    result = list([0])
    for calory in input:
        if calory == '':
            result.append(0)
        else:
            result[-1] += int(calory)
    return sorted(result, reverse=True)[0]
