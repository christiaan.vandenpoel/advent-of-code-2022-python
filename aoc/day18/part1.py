# Advent of Code - Day 18 - Part One
from .globe import neighbours, read_globe

def result(input):
    blocks = read_globe(input)

    total = 0

    for block in blocks:
        (x,y,z) = block
        for nb in neighbours(x, y, z):
            if nb not in blocks:
                total += 1

    return total
