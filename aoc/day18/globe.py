def neighbours(x,y,z):
    yield (x - 1, y, z)
    yield (x + 1, y, z)
    yield (x, y - 1, z)
    yield (x, y + 1, z)
    yield (x, y, z - 1)
    yield (x, y, z + 1)

def read_globe(input):
    blocks = []

    for line in input:
        (x,y,z) = [int(v) for v in line.split(",")]
        blocks.append((x,y,z))

    return blocks