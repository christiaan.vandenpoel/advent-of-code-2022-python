# Advent of Code - Day 18 - Part Two
from .globe import read_globe, neighbours
import math
from collections import deque

def result(input):
    blocks = read_globe(input)

    min_x = math.inf
    max_x = 0
    min_y = math.inf
    max_y = 0
    min_z = math.inf
    max_z = 0

    for (x,y,z) in blocks:
        min_x = min(x, min_x)
        max_x = max(x, max_x)
        min_y = min(y, min_y)
        max_y = max(y, max_y)
        min_z = min(z, min_z)
        max_z = max(z, max_z)

    min_x -= 1
    min_y -= 1
    min_z -= 1
    max_x += 1
    max_y += 1
    max_z += 1

    assert((min_x,min_y,min_z) not in blocks)
    assert((max_x,max_y,max_z) not in blocks)

    start = (min_x,min_y,min_z)
    water = set()
    water.add(start)

    queue = deque([start])
    while queue:
        last_water = queue.popleft()
        for (x,y,z) in neighbours(last_water[0], last_water[1], last_water[2]):
            if (x,y,z) in water:
                continue
            if x < min_x or x > max_x or y < min_y or y > max_y or z < min_z or z > max_z:
                continue
            if (x,y,z) in blocks:
                continue
            water.add((x,y,z))
            queue.append((x,y,z))

    total = 0

    for block in blocks:
        (x,y,z) = block

        for nb in neighbours(x, y, z):
            if nb in water:
                total += 1

    return total

    




