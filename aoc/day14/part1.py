# Advent of Code - Day 14 - Part One
def result(input):
    cave_map = dict()
    sand_count = 0
    max_y = 0
    
    # assemble map
    for line in input:
        coordinates = [tuple(list(map(int, string.split(",")))) for string in line.split(" -> ")]
        for split in [coordinates[idx:idx+2] for idx in range(0,len(coordinates)-1)]:
            split = sorted(split)
            (x1,y1) = split[0]
            (x2,y2) = split[1]
            for (x,y) in [(x, y) for x in range(x1, x2 + 1) for y in range(y1, y2 + 1)]:
                max_y = max(y, max_y)
                cave_map[(x,y)] = '#'

    # let the sand go
    while True:
        sand_count += 1
        (x,y) = (500,0)
        while True:
            if y > max_y:
                return sand_count - 1
            down = (x,y+1)
            down_left = (x -1, y + 1)
            down_right = (x + 1, y + 1)

            if down not in cave_map:
                (x,y) = down
            elif down_left not in cave_map:
                (x,y) = down_left
            elif down_right not in cave_map:
                (x,y) = down_right
            else:
                cave_map[(x,y)] = 'o'
                break
    