# Advent of Code - Day 23 - Part Two
from .elves import parse, print_grid, move_elves
import math

def result(input):
    elves = parse(input)

    direction_to_consider = 0
    round = 1
    while True:
        new_elves = move_elves(elves, direction_to_consider)
        if new_elves == elves:
            return round
        elves = new_elves
        direction_to_consider = (direction_to_consider + 1) % 4
        round += 1

