from collections import defaultdict
from dataclasses import dataclass
import math
import time

@dataclass(init=True, frozen=True)
class Coordinate():
    x: int
    y: int

    def __repr__(self) -> str:
        return f"Coordinate(x:{self.x} y:{self.y})"

CACHE: dict[Coordinate, list[Coordinate]] = dict()

def print_grid(elves: set[Coordinate]):
    min_x = min_y = math.inf
    max_x = max_y = 0
    for elf in elves:
        min_x = min(elf.x, min_x)
        min_y = min(elf.y, min_y)
        max_x = max(elf.x, max_x)
        max_y = max(elf.y, max_y)

    print(f" ----- elves ------")
    for y in range(min_y, max_y+1):
        str = ""
        for x in range(min_x, max_x + 1):
            c = Coordinate(x,y)
            if c in elves:
                str += "#"
            else:
                str += "."
        print(str)

def parse(input) -> list[Coordinate]:
    elves: set[Coordinate] = set()
    for y, row in enumerate(input):
        for x, char in enumerate(row):
            p = Coordinate(x,y)
            if char == '#':
                elves.add(p)

    return elves

def adjecents(elf: Coordinate) -> list[Coordinate]:
    if elf in CACHE:
        return CACHE[elf]

    adjs = [
        Coordinate(elf.x - 1, elf.y - 1),
        Coordinate(elf.x, elf.y - 1),

        # east
        Coordinate(elf.x + 1, elf.y - 1),
        Coordinate(elf.x + 1, elf.y),

        #south
        Coordinate(elf.x + 1, elf.y + 1),
        Coordinate(elf.x, elf.y + 1),

        #west
        Coordinate(elf.x - 1, elf.y + 1),
        Coordinate(elf.x - 1, elf.y),
    ]
    CACHE[elf] = adjs
    return adjs

def move(elf: Coordinate, direction: int) -> Coordinate:
    # north
    if direction == 0:
        return Coordinate(elf.x, elf.y - 1)
    # south
    elif direction == 1:
        return Coordinate(elf.x, elf.y + 1)
    # west
    elif direction == 2:
        return Coordinate(elf.x - 1, elf.y)
    # east
    elif direction == 3:
        return Coordinate(elf.x + 1, elf.y)
    else:
        raise NotImplementedError(f"move: unsupported direction {direction}")

def move_elves(elves, direction_to_consider):
    proposals: dict[Coordinate, list[Coordinate]] = defaultdict(list)
    for elf in elves:
        # print(f"Processing elf: {elf}")
        adjs = list(adjecents(elf))
        north = adjs[0:3]
        east = adjs[2:5]
        south = adjs[4:7]
        west = adjs[6:] + adjs[:1]
        dirs = {
            0: north,
            1: south,
            2: west,
            3: east
        }
        if not any([adj in elves for adj in adjs]):
            proposals[elf].append(elf)
        elif not any([adj in elves for adj in dirs[direction_to_consider]]):
            proposals[move(elf, direction_to_consider)].append(elf)
        elif not any([adj in elves for adj in dirs[(direction_to_consider+1)%4]]):
            proposals[move(elf, (direction_to_consider+1)%4)].append(elf)
        elif not any([adj in elves for adj in dirs[(direction_to_consider+2)%4]]):
            proposals[move(elf, (direction_to_consider+2)%4)].append(elf)
        elif not any([adj in elves for adj in dirs[(direction_to_consider+3)%4]]):
            proposals[move(elf, (direction_to_consider+3)%4)].append(elf)
        else:
            proposals[elf].append(elf)

    new_elves = set()
    for new_location, prev_elves in proposals.items():
        if len(prev_elves) > 1:
            new_elves = new_elves.union(set(prev_elves))
        else:
            new_elves.add(new_location)
    return new_elves
