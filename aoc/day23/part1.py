# Advent of Code - Day 23 - Part One
from .elves import parse, print_grid, move_elves, Coordinate
import math
import time

def result(input, rounds):
    elves = parse(input)

    direction_to_consider = 0

    for _ in range(rounds):
        # do a round
        new_elves = move_elves(elves, direction_to_consider)
        elves = new_elves
        direction_to_consider = (direction_to_consider + 1) % 4
        # end of round
    
    min_x = min(elves, key=lambda x: x.x).x
    max_x = max(elves, key=lambda x: x.x).x
    min_y = min(elves, key=lambda x: x.y).y
    max_y = max(elves, key=lambda x: x.y).y
    
    return sum(Coordinate(x, y) not in elves for y in range(min_y, max_y + 1) for x in range(min_x, max_x + 1))
