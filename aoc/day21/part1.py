# Advent of Code - Day 21 - Part One
def create_operator(operation):

    add = operation.split(' + ')
    is_add = len(add) == 2
    minus = operation.split(' - ')
    is_minus = len(minus) == 2
    mul = operation.split(' * ')
    is_mul = len(mul) == 2
    div = operation.split(' / ')
    is_div = len(div) == 2

    if operation.isnumeric():
        return lambda m: int(operation)
    elif is_add:
        return lambda m: m[add[0]](m) + m[add[1]](m)
    elif is_minus:
        return lambda m: m[minus[0]](m) - m[minus[1]](m)
    elif is_mul:
        return lambda m: m[mul[0]](m) * m[mul[1]](m)
    elif is_div:
        return lambda m: m[div[0]](m) // m[div[1]](m)
    else:
        raise Exception("Unknown operation '{0}'".format(operation))

def result(input):
    monkeys = dict()

    for line in input:
        monkey, operation = line.split(": ")
        monkeys[monkey] = create_operator(operation)
    
    return monkeys['root'](monkeys)
