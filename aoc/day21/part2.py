# Advent of Code - Day 21 - Part Two
def create_operator(operation):

    add = operation.split(' + ')
    is_add = len(add) == 2
    minus = operation.split(' - ')
    is_minus = len(minus) == 2
    mul = operation.split(' * ')
    is_mul = len(mul) == 2
    div = operation.split(' / ')
    is_div = len(div) == 2

    if operation.isnumeric():
        return lambda m: int(operation)
    elif is_add:
        return lambda m: m[add[0]](m) + m[add[1]](m)
    elif is_minus:
        return lambda m: m[minus[0]](m) - m[minus[1]](m)
    elif is_mul:
        return lambda m: m[mul[0]](m) * m[mul[1]](m)
    elif is_div:
        return lambda m: m[div[0]](m) // m[div[1]](m)
    else:
        raise Exception("Unknown operation '{0}'".format(operation))

def result(input):
    monkeys = dict()

    for line in input:
        monkey, operation = line.split(": ")
        if monkey == 'root':
            ops = operation.split(' ')
            monkeys[monkey] = [ops[0],ops[-1]]
        else:
            monkeys[monkey] = create_operator(operation)

    left_value = monkeys[monkeys['root'][0]](monkeys)

    orig_humn = monkeys['humn']
    humn = monkeys['humn'](monkeys) + 10000

    monkeys['humn'] = lambda m: humn
    new_left_value = monkeys[monkeys['root'][0]](monkeys)

    impacted = ""
    other = ""

    if left_value != new_left_value:
        impacted = monkeys['root'][0]
        other = monkeys['root'][1]
    else:
        impacted = monkeys['root'][1]
        other = monkeys['root'][0]

    monkeys['humn'] = orig_humn

    last_human_value = new_human_value = -1

    if monkeys[impacted](monkeys) < monkeys[other](monkeys):
        comp = lambda : monkeys[impacted](monkeys) < monkeys[other](monkeys)
    else:
        comp = lambda : monkeys[impacted](monkeys) > monkeys[other](monkeys)

    while comp():
        last_human_value = monkeys['humn'](monkeys)
        new_human_value = last_human_value * 10
        monkeys['humn'] = lambda m: new_human_value

    while (new_human_value - last_human_value) // 10 > 0:
        diff = new_human_value - last_human_value
        divider = diff // 10
        monkeys['humn'] = lambda m: last_human_value

        while comp():
            last_human_value = monkeys['humn'](monkeys)
            new_human_value = last_human_value + divider
            monkeys['humn'] = lambda m: new_human_value

    diff = new_human_value - last_human_value
    divider = diff // 10
    monkeys['humn'] = lambda m: last_human_value

    for humn in range(last_human_value, new_human_value + 1):
        monkeys['humn'] = lambda m: humn
        if monkeys[impacted](monkeys) == monkeys[other](monkeys):
            return humn