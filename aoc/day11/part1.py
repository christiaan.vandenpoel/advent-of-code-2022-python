# Advent of Code - Day 11 - Part One
from aoc.day11.monkey_business import parse_monkeys, play_round


def result(input):
    monkeys = parse_monkeys(input)

    max_rounds = 20
    play_round(monkeys, max_rounds, True)

    s = sorted(monkeys, key=lambda m: m["items_processed"], reverse=True)

    return s[0]["items_processed"] * s[1]["items_processed"]
