from functools import reduce

def get_operation(operation):
    if operation == '*':
        return lambda x,y: x * y
    elif operation == '+':
        return lambda x,y: x + y
    else:
        raise Exception("unsupported operation '{0}'".format(operation))

def get_operator(operator):
    if operator == 'old':
        return lambda v: v
    elif operator.isnumeric():
        i = int(operator)
        return lambda v: i
    else:
        raise Exception("unsupported operator '{0}'".format(operator))

def parse_monkeys(input):
    input = "\n".join(input).split("\n\n")
    monkeys = list()

    for monkey_nr, monkey in enumerate(input):
        monkeys.append(dict())

        lines = monkey.splitlines()

        # Starting items: 79, 98
        starting_items = [int(v) for v in lines[1].strip().replace("Starting items: ", "").split(", ")] 
        monkeys[monkey_nr]["items"] = starting_items

        # Operation: new = old * 19
        operation = lines[2].strip().replace("Operation: new = ","").split(" ")
        monkeys[monkey_nr]["operation"] = get_operation(operation[1])
        monkeys[monkey_nr]["operators"] = []
        monkeys[monkey_nr]["operators"].append(get_operator(operation[0]))
        monkeys[monkey_nr]["operators"].append(get_operator(operation[2]))
        
        # Test: divisible by 23
        test = int(lines[3].strip().replace("Test: divisible by ", ""))
        monkeys[monkey_nr]["test"]=test

        # If true: throw to monkey 1
        test_is_true = int(lines[4].strip().replace("If true: throw to monkey ", ""))
        monkeys[monkey_nr]["test_is_true"]=test_is_true

        # If false: throw to monkey 3
        test_is_false = int(lines[5].strip().replace("If false: throw to monkey ", ""))
        monkeys[monkey_nr]["test_is_false"]=test_is_false

        monkeys[monkey_nr]["items_processed"] = 0

    return monkeys

def play_round(monkeys, max_rounds, decrease_worry=True):
    super_modulo = set(map(lambda m: m["test"], monkeys))
    super_modulo = reduce(lambda acc, v: acc * v, super_modulo)
    for round in range(max_rounds):
        for monkey in monkeys:
            for item in monkey["items"]:
                new_worry = monkey["operation"](monkey["operators"][0](item), monkey["operators"][1](item))
                if decrease_worry:
                    new_worry = new_worry // 3

                if new_worry % monkey["test"] == 0:
                    monkeys[monkey["test_is_true"]]["items"].append(new_worry % super_modulo)
                else:
                    monkeys[monkey["test_is_false"]]["items"].append(new_worry % super_modulo)

                monkey["items_processed"] += 1
            
            monkey["items"] = []