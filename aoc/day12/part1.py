# Advent of Code - Day 12 - Part One
from .bfs import text_to_grid, find_start_and_end, Point, breadth_first_search


def result(input):
    grid, max_row, max_col = text_to_grid(input)
    start, end = find_start_and_end(grid)
    start_point = Point(start[0], start[1])
    end_point = Point(end[0], end[1])
    steps_to_end = breadth_first_search(grid, start_point, end_point, max_row, max_col)
    return steps_to_end