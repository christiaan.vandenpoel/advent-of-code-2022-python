# Advent of Code - Day 12 - Part Two
from .bfs import text_to_grid, find_start_and_end, Point, breadth_first_search

def result(input):
    grid, max_row, max_col = text_to_grid(input)
    start, end = find_start_and_end(grid)
    start_point = Point(start[0], start[1])
    end_point = Point(end[0], end[1])

    starting_points = [Point(x,y) for x in range(0, max_col) for y in range(0,max_row) if grid[(x,y)] in ['a', 'S']]
    distances = [breadth_first_search(grid, sp, end_point, max_row, max_col) for sp in starting_points if breadth_first_search(grid, sp, end_point, max_row, max_col) > 0]

    return sorted(distances)[0]