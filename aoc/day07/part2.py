# Advent of Code - Day 7 - Part Two
from aoc.day07.tree_parser import parse

def result(input):
    root_dir = parse(input)
    return root_dir.cleanup(70000000, 30000000)
