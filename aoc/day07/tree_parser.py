class Tree:
    pass

class Dir:
    def __init__(self, name, parent = None) -> None:
        self.name = name
        self.files = []
        self.dirs = dict()
        self.parent = parent
        self.my_files_size = 0

    def add_new_dir(self, name):
        new_dir = Dir(name, self)
        self.dirs[name] = new_dir

    def add_file(self, name, size):
        self.files.append(File(name, size))
        self.my_files_size += size

    def dir_into(self,name):
        return self.dirs[name]
    
    def go_up(self):
        return self.parent

    def sum_of_total_sizes(self, maximum):
        total = 0
        if self.size() <= maximum:
            total += self.size()
        for dir in self.dirs.values():
            total += dir.sum_of_total_sizes(maximum)
        return total

    def size(self):
        return self.my_files_size + sum(dir.size() for dir in self.dirs.values())

    def cleanup(self, maximum, margin):
        extra_space_needed = margin - (maximum - self.size())
        # find all dirs with size >= extra_space_needed
        # take the smallest
        # return that size
        dirs = []
        self.add_if_larger_than(extra_space_needed, dirs)
        return sorted(dirs, key=lambda d: d.size())[0].size()

    def add_if_larger_than(self, minimum, dirs):
        if self.size() >= minimum:
            dirs.append(self)
        for dir in self.dirs.values():
            dir.add_if_larger_than(minimum, dirs)

    def __repr__(self):
        return("Dir(name='{0}' size={3} dir cnt={1} file cnt={2})".format(self.name, len(self.dirs), len(self.files), self.size()))

class File:
    def __init__(self, name, size):
        self.name = name
        self.size = size

def parse(lines):
    root_dir = current_dir = Dir("/")
    for line in lines:
        if line[0] == '$':
            # command: 
            # cd /
            # cd <dir>
            # cd ..
            if line == '$ ls':
                continue
            dest = line[5:]
            if dest == '/':
                pass
            elif dest == '..':
                current_dir = current_dir.go_up()
            else:
                current_dir = current_dir.dir_into(dest)
        elif line[0:3] == 'dir':
            dest = line[4:]
            # current directory contains another dir
            current_dir.add_new_dir(dest)
        else:
            # file list: 8033020 d.log
            size, name = line.split(" ")
            size = int(size)
            current_dir.add_file(name, size)
    return root_dir