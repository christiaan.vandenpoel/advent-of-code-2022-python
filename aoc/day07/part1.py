# Advent of Code - Day 7 - Part One
from aoc.day07.tree_parser import parse

def result(input):
    root_dir = parse(input)
    return root_dir.sum_of_total_sizes(100000)
