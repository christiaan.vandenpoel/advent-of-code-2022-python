from collections import defaultdict
import heapq

def print_winds(winds, max_x, max_y):
    for row in range(max_y+1):
        str = ""
        for col in range(max_x+1):
            chars = winds[(col, row)]
            if (l := len((chars))) > 1:
                str += f"{l}"
            elif l == 1:
                str += chars[0]
            else:
                str += '.'
        print(str)
    print()

def process_winds(winds, max_x, max_y):
    new_winds = dict()

    width = max_x + 1
    height = max_y + 1 

    for row in range(max_y + 1):
        for col in range(max_x + 1):
            chars = winds[(col, row)]
            for char in chars:
                if char == '>':
                    new_col = (col+1)%width
                    if (new_col, row) not in new_winds:
                        new_winds[(new_col, row)] = []
                    new_winds[(new_col,row)].append(char)
                elif char == '<':
                    new_col = (col-1+width)%width
                    if (new_col, row) not in new_winds:
                        new_winds[(new_col, row)] = []
                    new_winds[(new_col,row)].append(char)
                elif char == 'v':
                    new_row = (row+1)%height
                    if (col, new_row) not in new_winds:
                        new_winds[(col, new_row)] = []
                    new_winds[(col,new_row)].append(char)
                elif char == '^':
                    new_row = (row-1+height)%height
                    if (col, new_row) not in new_winds:
                        new_winds[(col, new_row)] = []
                    new_winds[(col,new_row)].append(char)
                else:
                    pass

    for x in range(max_x+1):
        for y in range(max_y+1):
            if (x,y) not in new_winds:
                new_winds[(x,y)] = ['.']

    return new_winds

def parse(input):
    winds = dict()
    for y, line in enumerate(input[1:-1]):
        for x, char in enumerate(line[1:-1]):
            pos = (x,y)
            if pos not in winds:
                winds[pos] = []
            winds[(x,y)].append(char)

    max_x = max([x for (x,_) in winds.keys()])
    max_y = max([y for (_,y) in winds.keys()])

    return winds, max_x, max_y

def bfs(winds, start, destination, max_x, max_y, start_minute=0):
    queue = [
        (start_minute, start)
    ]

    visited = set()
    visited.add(queue[0])

    while True:
        new_winds = process_winds(winds, max_x, max_y)

        next_queue = []
        
        while queue:
            time_passed, current_path = heapq.heappop(queue)
            # print(f" processing: tp:{time_passed} p:{current_path}")
            if current_path == destination:
                return time_passed, winds

            next_candidates = [(current_path[0]+dx, current_path[1]+dy) 
                                for dx, dy in [(-1, 0), (0, -1), (0, 1), (1, 0), (0, 0)]]
            # print(f" next candidates: {next_candidates}")
            for candidate in next_candidates:
                if candidate == start or candidate == destination:
                    if (time_passed+1, candidate) not in visited:
                        heapq.heappush(next_queue,(time_passed + 1, candidate))
                        visited.add((time_passed + 1, candidate))
                if candidate not in new_winds:
                    continue
                if new_winds[candidate] == ['.']:
                    # print(f" seems a good candidate {candidate} ({winds[candidate]})")
                    if (time_passed+1, candidate) not in visited:
                        heapq.heappush(next_queue,(time_passed + 1, candidate))
                        visited.add((time_passed + 1, candidate))
        winds = new_winds
        queue = next_queue  
