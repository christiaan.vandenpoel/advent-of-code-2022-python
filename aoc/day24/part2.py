# Advent of Code - Day 24 - Part Two
from .blizzard import parse, bfs

def result(input):
    winds, max_x, max_y = parse(input)

    start = (0,-1)
    destination = (max_x, max_y+1)
    
    once, winds = bfs(winds, start, destination, max_x, max_y)
    twice, winds = bfs(winds, destination, start, max_x, max_y, once)
    thrice, winds = bfs(winds, start, destination, max_x, max_y, twice)

    return thrice
