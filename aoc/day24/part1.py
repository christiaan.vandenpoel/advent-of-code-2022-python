# Advent of Code - Day 24 - Part One
from .blizzard import parse, bfs

def result(input):
    winds, max_x, max_y = parse(input)

    start = (0,-1)
    destination = (max_x, max_y+1)
    
    timepassed, _ = bfs(winds, start, destination, max_x, max_y)
    return timepassed
