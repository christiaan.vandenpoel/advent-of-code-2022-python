# Advent of Code - Day 13 - Part One
from .distress_signal import is_in_right_order

def result(input):
    check = 0
    pairs = "\n".join(input).split("\n\n")
    for idx, pair in enumerate(pairs):
        first, second = pair.split("\n")
        first = eval(first)
        second = eval(second)

        r = is_in_right_order(first, second)
        if r == True:
            check += idx + 1
        elif r == None:
            raise("Expected True or False, got None")

    return check
