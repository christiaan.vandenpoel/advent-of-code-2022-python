def is_in_right_order(first, second):
    for idx, f in enumerate(first):
        if idx >= len(second):
            return False

        s = second[idx]
        if type(f) is list and type(s) is int:
            r =  is_in_right_order(f, [s])
            if r is not None:
                return r
        elif type(f) is int and type(s) is list:
            r = is_in_right_order([f], s)
            if r is not None:
                return r
        elif type(f) is list and type(s) is list:
            r = is_in_right_order(f, s)
            if r is not None:
                return r
        else:
            if f < s:
                return True
            elif f > s:
                return False

    if len(first) < len(second):
        return True
    elif len(first) > len(second):
        return False
    else:
        return None
