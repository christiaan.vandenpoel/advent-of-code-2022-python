# Advent of Code - Day 13 - Part Two
from .distress_signal import is_in_right_order

class Packet():
    def __init__(self, packet) -> None:
        self.packet = packet

    def __lt__(self, other):
        return is_in_right_order(self.packet, other.packet)
        raise NotImplementedError

    def __repr__(self) -> str:
        return "Packet({0})".format(self.packet)

def result(input):
    packets = [Packet(eval(line)) for line in "\n".join(input).replace("\n\n", "\n").splitlines()]
    packets.append(Packet([[2]]))
    packets.append(Packet([[6]]))
    packets = [p.packet for p in sorted(packets)]
    i1 = packets.index([[2]]) + 1
    i2 = packets.index([[6]]) + 1

    return i1 * i2
