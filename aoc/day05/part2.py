# Advent of Code - Day 5 - Part Two
from aoc.utils import supply_stack

def result(input):
    state, movements = supply_stack.parse_stack_and_movements(input)

    return supply_stack.move_all_at_once(state, movements)