# Advent of Code - Day 5 - Part One
from aoc.utils import supply_stack

def result(input):
    state, movements = supply_stack.parse_stack_and_movements(input)

    return supply_stack.move_one_by_one(state, movements)
