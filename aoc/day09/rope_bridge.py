
class RopeBridge():
    def __init__(self, size = 2) -> None:
        self._rope = [(0,0)] * size
        self._tail_trail = set()
        self._size = size

    def move(self, movements):
        for movement in movements:
            direction, steps = movement.split(" ")
            steps = int(steps)
            for step in range(1, steps + 1):
                (x_head,y_head) = self.head()
                if direction == 'R':
                    x_head += 1    
                elif direction == 'U':
                    y_head += 1
                elif direction == 'L':
                    x_head -= 1    
                elif direction == 'D':
                    y_head -= 1
                self.set_head(x_head, y_head)

                for idx in range(0, self._size - 1):
                    current = self._rope[idx]
                    follower = self._rope[idx+1]
    
                    (x_follower, y_follower) = self._advance_second(current, follower)
                    self._rope[idx+1] = (x_follower, y_follower)

                self._tail_trail.add(self.tail())
        pass
        

    def head(self):
        return self._rope[0]

    def set_head(self, x, y):
        self._rope[0] = (x, y)

    def tail(self):
        return self._rope[-1]
    
    def set_tail(self, x, y):
        self._rope[-1] = (x, y)

    def tail_trail(self):
        return self._tail_trail

    def distance(self, first, second):
        (x_first, y_first) = first
        (x_second, Y_second) = second
        return abs(x_second - x_first) + abs(Y_second - y_first)

    def _advance_second(self, first, second):
        (x_first, y_first) = first
        (x_second, y_second) = second
        if self.distance(first,second) in [0,1]:
            pass
        elif self.distance(first, second) == 3:
            if abs(y_first - y_second) == 1:
                x_second = x_first - (x_first - x_second)//2
                y_second = y_first
            elif abs(x_first - x_second) == 1:
                x_second = x_first    
                y_second = y_first - (y_first - y_second)//2
        elif self.distance(first, second) == 2:
            if y_first == y_second:
                x_second = x_first - (x_first - x_second)//2
            elif x_first == x_second:
                y_second = y_first - (y_first - y_second)//2
        elif self.distance(first, second) == 4:
            x_second = x_first - (x_first - x_second)//2
            y_second = y_first - (y_first - y_second)//2
        else:
            raise Exception("unsupport distance {0}".format(self.distance(first, second)))

        return (x_second, y_second)
