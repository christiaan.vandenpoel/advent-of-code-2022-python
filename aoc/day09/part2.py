# Advent of Code - Day 9 - Part Two
from aoc.day09.rope_bridge import RopeBridge

def result(input):
    rope_bridge = RopeBridge(10)
    rope_bridge.move(input)
    return len(rope_bridge.tail_trail())
