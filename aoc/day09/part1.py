# Advent of Code - Day 9 - Part One
from aoc.day09.rope_bridge import RopeBridge

def result(input):
    rope_bridge = RopeBridge()
    rope_bridge.move(input)
    return len(rope_bridge.tail_trail())
