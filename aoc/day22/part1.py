# Advent of Code - Day 22 - Part One
import re
from .monkey_map import direction_change, next_position, print_board, parse_board

def result(input):
    inputmap, instructions = "\n".join(input).split("\n\n")
    inputmap = inputmap.splitlines()

    board, start_position, max_x, max_y = parse_board(inputmap)

    board[start_position] = '>'
    current_direction = '>'  # >, v, <, ^
    current_position = start_position

    while len(instructions) > 0:
        move = None
        change_direction = None
        if m := re.match(r'^\d+', instructions):
            move = int(m[0])
        elif m := re.match(r'^\w\d+', instructions):
            change_direction = m[0][0]
            move = int(m[0][1:])
        instructions = instructions[len(m[0]):]

        if change_direction is not None:
            current_direction = direction_change(current_direction, change_direction)
            board[current_position] = current_direction

        for m in range(move):
            next_pos = next_position(current_position, current_direction)

            if board[next_pos] == ' ':
                if current_direction == '>':
                    reverted_dir = "<"
                elif current_direction == "v":
                    reverted_dir = "^"
                elif current_direction == "<":
                    reverted_dir = ">"
                else:
                    reverted_dir = "v"
                
                this_pos = current_position
                while board[next_position(this_pos, reverted_dir)] != ' ':
                    this_pos = next_position(this_pos, reverted_dir)
                next_pos = this_pos

            if board[next_pos] not in [' ', '#']:
                current_position = next_pos
                board[current_position] = current_direction
            elif board[next_pos] == '#':
                break

    direction_value_map = {
        ">": 0,
        "v": 1,
        "<": 2,
        "^": 3
    }
    return 1000 * (current_position[1] + 1) + 4 * (current_position[0] + 1) + direction_value_map[current_direction]
