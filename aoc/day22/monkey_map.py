from collections import defaultdict
from colorama import Fore, Style
from PIL import Image, ImageDraw, ImageFont

def parse_board(input):
    board = defaultdict(lambda : ' ')
    start_position = None
    max_y = max_x = 0

    for y, row in enumerate(input):
        max_y = max(max_y, y)
        for x, char in enumerate(row):
            max_x = max(x, max_x)
            if start_position is None and char != ' ':
                start_position = (x,y)
            board[(x,y)] = char

    return board, start_position, max_x, max_y

def print_board(board, max_x, max_y, images=[], text="", current=None):
    img = Image.new('RGB', (max_x*5, max_y*5), (223,227,228))
    draw = ImageDraw.Draw(img)
    font = ImageFont.load_default()
    draw.text((5,5), text, font=font, fill='black')
    for y in range(0, max_y+1):
        line = ""
        for x in range(0, max_x+1):
            if board[(x,y)] == ' ':
                pass
            elif (x,y) == current:
                draw.rectangle([(x*5,y*5), (x*5+4,y*5+4)], fill='white')
            elif board[(x,y)] == '.':
                draw.rectangle([(x*5,y*5), (x*5+4,y*5+4)], fill=(174,217,228))
            elif board[(x,y)] == '#':
                draw.rectangle([(x*5,y*5), (x*5+4,y*5+4)], fill=(255,0,0))
            else:
                draw.rectangle([(x*5,y*5), (x*5+4,y*5+4)], fill=(0,0, 255))

            # if board[(x,y)] in [' ', '.', '#']:
            #     line += board[(x,y)]    
            # else:
            #     line += Fore.WHITE + Style.BRIGHT + board[(x,y)] + Style.RESET_ALL + Fore.RESET
        # print(line)
    images.append(None)
    img.save("tmp/board {0:05d}.png".format(len(images)), format='png')
    return images

def save_as_gif(images):
    images[0].save('board.gif',
              save_all=True, format='GIF', append_images=images[1:], optimize=False, duration=40, loop=0)
    pass

def direction_change(current_direction, change):
    mapping = {
        ">": {
            "L": "^",
            "R": "v",
        },
        "v": {
            "L": ">",
            "R": "<"
        },
        "<": {
            "L": "v",
            "R": "^"
        },
        "^": {
            "L": "<",
            "R": ">"
        }
    }
    return mapping[current_direction][change]

def next_position(current_position, direction):
    x,y = current_position
    if direction == ">":
        return (x+1,y)
    elif direction == "v":
        return (x, y + 1)
    elif direction == "<":
        return (x - 1, y)
    elif direction == "^":
        return (x, y - 1)
    else:
        raise Exception("Unsupported direction '{0}'".format(direction))
