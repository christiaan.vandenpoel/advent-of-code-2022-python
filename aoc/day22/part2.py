# Advent of Code - Day 22 - Part Two
import re
from .monkey_map import direction_change, print_board, parse_board, save_as_gif
from math import gcd

def next_position_on_cube(board, position, direction, size, off_board_func):
    x,y = position
    if direction == ">":
        (new_x,new_y) = (x+1,y)
    elif direction == "v":
        (new_x,new_y) = (x, y + 1)
    elif direction == "<":
        (new_x,new_y) = (x - 1, y)
    elif direction == "^":
        (new_x,new_y) = (x, y - 1)
    else:
        raise Exception("Unsupported direction '{0}'".format(direction))

    new_x, new_y, direction = off_board_func(position, size, new_x, new_y, direction, board)

    
    return (new_x, new_y), direction

def result(input, off_board_func):
    images = []
    inputmap, instructions = "\n".join(input).split("\n\n")
    inputmap = inputmap.splitlines()

    board, start_position, max_x, max_y = parse_board(inputmap)

    board[start_position] = '>'
    current_direction = '>'  # >, v, <, ^
    current_position = start_position

    size = gcd((max_x + 1), (max_y + 1))

    while len(instructions) > 0:
        move = None
        change_direction = None
        if m := re.match(r'^\d+', instructions):
            move = int(m[0])
        elif m := re.match(r'^\w\d+', instructions):
            change_direction = m[0][0]
            move = int(m[0][1:])
        instructions = instructions[len(m[0]):]

        # print("Moving {0}{1}".format(change_direction, move))

        if change_direction is not None:
            current_direction = direction_change(current_direction, change_direction)
            board[current_position] = current_direction

        # print("{"*20)
        # print_board(board, max_x, max_y)
        # print("}"*20)

        for m in range(move):
            next_pos, new_direction = next_position_on_cube(board, current_position, current_direction, size, off_board_func)

            if board[next_pos] not in [' ', '#']:
                current_position = next_pos
                board[current_position] = new_direction
                current_direction = new_direction
                # print("  pos:{0} dir:{1} cnt:{2}".format(current_position, current_direction, m))
            elif board[next_pos] == '#':
                # print("  blocked at next pos: {0}, therefor staying at pos:{1} dir:{2} cnt:{3}".format(next_pos, current_position, current_direction, m))
                break

            # print_board(board, max_x, max_y, images, current=current_position, text="{0}{1}\n{2}".format(change_direction, move, current_position))

    # print_board(board, max_x, max_y, images)

    # print_board(board, max_x, max_y)

    direction_value_map = {
        ">": 0,
        "v": 1,
        "<": 2,
        "^": 3
    }
    return 1000 * (current_position[1] + 1) + 4 * (current_position[0] + 1) + direction_value_map[board[current_position]]
