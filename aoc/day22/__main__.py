#!/usr/bin/env python3

from pathlib import Path

from aoc.day22 import part1, part2

def off_board_func(position, size, new_x, new_y, direction, board):
    orig_direction = direction
    x,y = position

    # check if we drop of the board
    if board[(new_x, new_y)] == ' ':
        # print("Dropping of the board - from: {0} to {1} ({2})".format((x,y), (new_x, new_y), direction))
        if x == 50 and y in range(0,50) and direction == "<":
            direction = ">"
            new_x = 0
            new_y = 149 - y % size
            # print(" (jump) 01: B1 <  -  A3 >  ({0}{2} to {1}{3})".format((x,y), (new_x, new_y), orig_direction, direction))
        elif y == 199 and x in range(0, 50) and direction == "v":
            direction = "v"
            new_x = 100 + x % size
            new_y = 0
            # print(" (jump) 02: A4 v  -  C1 v  ({0}{2} to {1}{3})".format((x,y), (new_x, new_y), orig_direction, direction))
        elif y == 0 and x in range(100, 150) and direction == "^":
            direction = "^"
            new_y = 199
            new_x = 0 + x % size
            # print(" (jump) 03: C1 ^  -  A4 ^  ({0}{2} to {1}{3})".format((x,y), (new_x, new_y), orig_direction, direction))
        elif y == 149 and x in range(50, 100) and direction == "v":
            direction = "<"
            new_x = 49
            new_y = 150 + x % size
            # print(" (jump) 04: B3 v  -  A4 <  ({0}{2} to {1}{3})".format((x,y), (new_x, new_y), orig_direction, direction))
        elif x == 49 and y in range(150, 200) and direction == ">":
            direction = "^"
            new_x = 50 + y % size
            new_y = 149
            # print(" (jump) 05: A4 >  -  B3 ^ ({0}{2} to {1}{3})".format((x,y), (new_x, new_y), orig_direction, direction))
        elif y == 100 and x in range(0,50) and direction == "^":
            direction = ">"
            new_x = 50
            new_y = 50 + x % size
            # print(" (jump) 06: A3 ^  -  B2 > ({0}{2} to {1}{3})".format((x,y), (new_x, new_y), orig_direction, direction))
        elif x == 50 and y in range(50, 100) and direction == "<":
            direction = "v"
            new_x = y % size
            new_y = 100
            # print(" (jump) 07: B2 <  -  A3 v ({0}{2} to {1}{3})".format((x,y), (new_x, new_y), orig_direction, direction))
        elif x == 0 and y in range(100,150) and direction == "<":
            direction = ">"
            new_x = 50
            new_y = 49 - y % size
            # print(" (jump) 08: A3 <  -  B1 > ({0}{2} to {1}{3})".format((x,y), (new_x, new_y), orig_direction, direction))
        elif x == 149 and y in range(0,50) and direction == ">":
            direction = "<"
            new_x = 99
            new_y = 149 - y % size
            # print(" (jump) 09: C1 >  -  B3 < ({0}{2} to {1}{3})".format((x,y), (new_x, new_y), orig_direction, direction))
        elif x == 0 and y in range(150, 200) and direction == "<":
            direction = "v"
            new_x = 50 + y % size
            new_y = 0
            # print(" (jump) 10: A4 <  -  B1 v ({0}{2} to {1}{3})".format((x,y), (new_x, new_y), orig_direction, direction))
        elif y == 0 and x in range(50, 100) and direction == "^":
            direction = ">"
            new_x = 0 
            new_y = 150 + x % size
            # print(" (jump) 11: B1 ^  -  A4 > ({0}{2} to {1}{3})".format((x,y), (new_x, new_y), orig_direction, direction))
        elif y == 49 and x in range(100, 150) and direction == "v":
            direction = "<"
            new_x = 99
            new_y = 50 + x % size
            # print(" (jump) 12: C1 v  -  B2 < ({0}{2} to {1}{3})".format((x,y), (new_x, new_y), orig_direction, direction))
        elif x == 99 and y in range(50, 100) and direction == ">":
            direction = "^"
            new_x = 100 + y % size
            new_y = 49
            # print(" (jump) 13: B2 >  -  C1 ^ ({0}{2} to {1}{3})".format((x,y), (new_x, new_y), orig_direction, direction))
        elif x == 99 and y in range(100, 150) and direction == ">":
            direction = "<"
            new_x = 149
            new_y = 49 - y % size
            # print(" (jump) 14: B3 >  -  C1 < ({0}{2} to {1}{3})".format((x,y), (new_x, new_y), orig_direction, direction))
        else:
            raise NotImplementedError("unsupported {0} > {1} '{2}'".format((x,y), (new_x, new_y), direction))

    return new_x, new_y, direction

def read_file(filename):
    path = Path(__file__).parent.resolve()
    with open(path / filename, 'r') as f:
        lines = f.read().splitlines()
        return lines

def main():
    input = read_file("./resources/input.txt")

    print("--- Part One ---")
    print("Result:", part1.result(input))

    print("--- Part Two ---")
    print("Result:", part2.result(input, off_board_func))

if __name__ == "__main__":
    main()
