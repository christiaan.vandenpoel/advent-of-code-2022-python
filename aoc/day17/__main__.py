#!/usr/bin/env python3

from pathlib import Path

from aoc.day17 import part1, part2

def read_file(filename):
    path = Path(__file__).parent.resolve()
    with open(path / filename, 'r') as f:
        lines = f.read().splitlines()
        return lines

def main():
    input = read_file("./resources/input.txt")

    print("--- Part One ---")
    print("Result:", part1.result(input, total_rocks=2022))

    print("--- Part Two ---")
    total_rocks = 1_000_000_000_000
    print("Result:", part1.result(input, total_rocks=total_rocks))

if __name__ == "__main__":
    main()
