# Advent of Code - Day 17 - Part Two

# def pbar(window):
#     for i in range(10):
#         window.addstr(10, 10, "[" + ("=" * i) + ">" + (" " * (10 - i )) + "]")
#         window.refresh()
#         time.sleep(0.5)

# curses.wrapper(pbar)

# |..@@@@.|
# |.......|
# |...@...|
# |..@@@..|
# |...@...|
# |.......|
# |....@..|
# |....@..|
# |..@@@..|
# |.......|
# |..@....|
# |..@....|
# |..@....|
# |..@....|
# |.......|
# |..@@...|
# |..@@...|

MINUS = [
    0b0011110
]
PLUS = [
    0b0001000,
    0b0011100,
    0b0001000
]
J = list(reversed([
    0b0000100,
    0b0000100,
    0b0011100
]))
I = [
    0b0010000,
    0b0010000,
    0b0010000,
    0b0010000,
]
BLOCK = [
    0b0011000,
    0b0011000,
]
rocks = [
    MINUS,
    PLUS,
    J,
    I,
    BLOCK
]
current_rock=0

def print_stack(stack, char="#"):
    str = ""
    for row in reversed(stack):
        str = "|"
        mask = 0b0100_0000
        while mask:
            if row & mask > 0:
                str += char
            else:
                str += "."
            mask >>= 1
        str += "|"
        print(str)
    print("+-------+")

def get_value_for_stack(stack, idx):
    if idx >= len(stack) or idx < 0:
        return 0
    else:
        return stack[idx]

def next_rock(rocks):
    while True:
        for rock in rocks:
            yield rock

class RockFalling():
    def __init__(self, stack, rock) -> None:
        self.stack = stack
        self._rock = rock
        self.offset = len(stack) + 3
        self.working = rock.copy()

    def process_jet(self, jet):
        new_working = self.working
        if jet == '<' and all([row & 0b01000000 == 0 for row in self.working]):
            new_working = [row << 1 for row in self.working]
        elif jet == '>' and all([row & 0b01 == 0 for row in self.working]):
            new_working = [row >> 1 for row in self.working]
        if all([ get_value_for_stack(self.stack, idx + self.offset) & row == 0 for idx, row in enumerate(new_working)]):
            self.working = new_working
        
    def go_down(self):
        if self.offset == 0:
            return False

        can_move_down = True
        for idx,row in enumerate(self.working):
            if get_value_for_stack(self.stack, self.offset + idx - 1) & row != 0:
                can_move_down = False

        if can_move_down:
            self.offset -= 1
            return True
        else:
            return False
        

    def items(self):
        for item in self.working:
            yield item


def result(input, total_rocks=0, current_jet=0):
    jets = input[0]
    stack = list()
    stack_height = 0
    seen = dict()
    progression = dict()

    for rock_count, rock in enumerate(next_rock(rocks)):
        if rock_count == 2022:
            break
        if rock_count >= total_rocks:
            break
        rock_falling = RockFalling(stack, rock)

        while True:
            jet = jets[current_jet]
            current_jet += 1            
            current_jet %= len(jets)
            rock_falling.process_jet(jet)
            if not rock_falling.go_down():
                break
        
        for item_idx, row in enumerate(rock_falling.items()):
            total = rock_falling.offset + item_idx
            if total >= len(stack):
                stack_height += 1
                stack.append(row)
            else:
                stack[total] |= row

        progression[rock_count] = stack_height

        tmp = 0
        while tmp < len(stack) and stack[tmp] != 0:
            tmp += 1

        for lengths in range(1, len(stack) // 2):
            if stack[-lengths:] == stack[-lengths*2:-lengths]:
                print(f"found a repitition ({lengths}): {stack[-lengths:]}")
                print(f"len(stack)={len(stack)} rock_count={rock_count}")

        assert tmp == stack_height

    stack_height = 0
    while stack_height < len(stack) and stack[stack_height] != 0:
        stack_height += 1

    return stack_height

