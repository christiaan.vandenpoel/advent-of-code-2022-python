# Advent of Code - Day 19 - Part One
import re
from dataclasses import dataclass
from collections import deque

PATTERN = re.compile(
    r"Blueprint (?P<blueprint>\d+): "
    r"Each ore robot costs (?P<ore_robot_ore_cost>\d+) ore. Each clay robot costs (?P<clay_robot_ore_cost>\d+) ore. "
    r"Each obsidian robot costs (?P<obsidian_robot_ore_cost>\d+) ore and (?P<obsidian_robot_clay_cost>\d+) clay. "
    r"Each geode robot costs (?P<geode_robot_ore_cost>\d+) ore and (?P<geode_robot_obsidian_cost>\d+) obsidian."
)


@dataclass(init=True)
class Blueprint():
    number: int
    ore_robot_ore_cost: int
    clay_robot_ore_cost: int
    obsidian_robot_ore_cost: int
    obsidian_robot_clay_cost: int
    geode_robot_ore_cost: int
    geode_robot_obsidian_cost: int

    @classmethod
    def from_line(cls, line):
        match = PATTERN.match(line)
        blueprint = int(match['blueprint'])
        ore_robot_ore_cost = int(match['ore_robot_ore_cost'])
        clay_robot_ore_cost = int(match['clay_robot_ore_cost'])
        obsidian_robot_ore_cost = int(match['obsidian_robot_ore_cost'])
        obsidian_robot_clay_cost = int(match['obsidian_robot_clay_cost'])
        geode_robot_ore_cost = int(match['geode_robot_ore_cost'])
        geode_robot_obsidian_cost = int(match['geode_robot_obsidian_cost'])
        return cls(blueprint, ore_robot_ore_cost, clay_robot_ore_cost, obsidian_robot_ore_cost, obsidian_robot_clay_cost, geode_robot_ore_cost, geode_robot_obsidian_cost)


class Traverse():
    blueprint: Blueprint
    ore_count: int
    clay_count: int
    obsidian_count: int
    geode_count: int
    ore_bots: int
    clay_bots: int
    obsidian_bots: int
    geode_bots: int
    time_spent: int

    def __init__(self,
                 blueprint=None,
                 ore_count=0,
                 clay_count=0,
                 obsidian_count=0,
                 geode_count=0,
                 ore_bots=0,
                 clay_bots=0,
                 obsidian_bots=0,
                 geode_bots=0,
                 time_spent=0) -> None:
        self.blueprint = blueprint
        self.ore_count = ore_count
        self.clay_count = clay_count
        self.obsidian_count = obsidian_count
        self.geode_count = geode_count
        self.geode_bots = geode_bots
        self.clay_bots = clay_bots
        self.obsidian_bots = obsidian_bots
        self.ore_bots = ore_bots
        self.time_spent = time_spent

    def has_time_left(self, minutes):
        return self.time_spent < minutes

    def id(self):
        return (self.ore_count, self.clay_count, self.obsidian_count, self.geode_count, self.ore_bots, self.clay_bots, self.obsidian_bots, self.geode_bots)

    def next_traverses(self):
        max_needed_ore_per_minute = max(self.blueprint.clay_robot_ore_cost,
                                        self.blueprint.obsidian_robot_ore_cost, self.blueprint.geode_robot_ore_cost)
        max_needed_clay_per_minute = self.blueprint.obsidian_robot_clay_cost
        max_needed_obsidian_per_minute = self.blueprint.geode_robot_obsidian_cost

        if (self.blueprint.geode_robot_obsidian_cost <= self.obsidian_count
            and self.blueprint.geode_robot_ore_cost <= self.ore_count
            ):
            t = self.buy_geode_bot()
            yield t

        if (self.blueprint.obsidian_robot_clay_cost <= self.clay_count
                    and self.blueprint.obsidian_robot_ore_cost <= self.ore_count
                    and self.obsidian_bots < max_needed_obsidian_per_minute
                ):
            t = self.buy_obsidian_bot()
            yield t

        if (self.blueprint.clay_robot_ore_cost <= self.ore_count
                and self.clay_bots < max_needed_clay_per_minute
            ):
            t = self.buy_clay_bot()
            yield t

        if (self.blueprint.ore_robot_ore_cost <= self.ore_count
                and self.ore_bots < max_needed_ore_per_minute
            ):
            t = self.buy_ore_bot()
            yield t

        t = self.gather()
        # print(f" >> just collect the ores {t}")
        yield t

    def __repr__(self) -> str:
        return (
            f"Traverse(Ore:{self.ore_count}[{self.ore_bots}] "
            f"Clay:{self.clay_count}[{self.clay_bots}] "
            f"Obs:{self.obsidian_count}[{self.obsidian_bots}] "
            f"Geode:{self.geode_count}[{self.geode_bots}] "
            f"bp:{self.blueprint.number} ts:{self.time_spent}"
            ")"
        )

    def buy_geode_bot(self):
        return Traverse(
            blueprint=self.blueprint,
            ore_count=self.ore_count + self.ore_bots - self.blueprint.geode_robot_ore_cost,
            clay_count=self.clay_count + self.clay_bots,
            obsidian_count=self.obsidian_count + self.obsidian_bots -
            self.blueprint.geode_robot_obsidian_cost,
            geode_count=self.geode_count + self.geode_bots,
            ore_bots=self.ore_bots,
            clay_bots=self.clay_bots,
            obsidian_bots=self.obsidian_bots,
            geode_bots=self.geode_bots + 1,
            time_spent=self.time_spent + 1
        )

    def buy_obsidian_bot(self):
        return Traverse(
            blueprint=self.blueprint,
            ore_count=self.ore_count + self.ore_bots -
            self.blueprint.obsidian_robot_ore_cost,
            clay_count=self.clay_count + self.clay_bots -
            self.blueprint.obsidian_robot_clay_cost,
            obsidian_count=self.obsidian_count + self.obsidian_bots,
            geode_count=self.geode_count + self.geode_bots,
            ore_bots=self.ore_bots,
            clay_bots=self.clay_bots,
            obsidian_bots=self.obsidian_bots + 1,
            geode_bots=self.geode_bots,
            time_spent=self.time_spent + 1
        )

    def buy_clay_bot(self):
        return Traverse(
            blueprint=self.blueprint,
            ore_count=self.ore_count + self.ore_bots - self.blueprint.clay_robot_ore_cost,
            clay_count=self.clay_count + self.clay_bots,
            obsidian_count=self.obsidian_count + self.obsidian_bots,
            geode_count=self.geode_count + self.geode_bots,
            ore_bots=self.ore_bots,
            clay_bots=self.clay_bots + 1,
            obsidian_bots=self.obsidian_bots,
            geode_bots=self.geode_bots,
            time_spent=self.time_spent + 1
        )

    def buy_ore_bot(self):
        return Traverse(
            blueprint=self.blueprint,
            ore_count=self.ore_count + self.ore_bots - self.blueprint.ore_robot_ore_cost,
            clay_count=self.clay_count + self.clay_bots,
            obsidian_count=self.obsidian_count + self.obsidian_bots,
            geode_count=self.geode_count + self.geode_bots,
            ore_bots=self.ore_bots + 1,
            clay_bots=self.clay_bots,
            obsidian_bots=self.obsidian_bots,
            geode_bots=self.geode_bots,
            time_spent=self.time_spent + 1
        )

    def gather(self):
        return Traverse(
            blueprint=self.blueprint,
            ore_count=self.ore_count + self.ore_bots,
            clay_count=self.clay_count + self.clay_bots,
            obsidian_count=self.obsidian_count + self.obsidian_bots,
            geode_count=self.geode_count + self.geode_bots,
            ore_bots=self.ore_bots,
            clay_bots=self.clay_bots,
            obsidian_bots=self.obsidian_bots,
            geode_bots=self.geode_bots,
            time_spent=self.time_spent + 1
        )


def bfs(blueprint: Blueprint, minutes: int) -> int:
    print(blueprint)
    ore_bots_required = max(blueprint.clay_robot_ore_cost,
                            blueprint.geode_robot_ore_cost, blueprint.obsidian_robot_ore_cost)
    clay_bots_required = blueprint.obsidian_robot_clay_cost
    obsidian_bots_required = blueprint.geode_robot_obsidian_cost
    max_geode = 0

    start = (
        1,          # nr ore bots
        0,          # nr clay bots
        0,          # nr obsidian bots
        0,          # nr geode bots
        0,
        0,
        0,
    )
    visited = set([start])
    queue = [(1, 0, 0, 0, 0, 0, 0, 0)]

    for minute in range(1, minutes+1):
        print(f"-- minute {minute} ({len(queue)})")
        # if minute == 2:
        #     assert (1,0,0,0,1,0,0,0) in queue
        # if minute == 12:
        #     assert (1,3,1,0,2,4,0,0) in queue
        new_queue = []
        while queue:
            path = queue.pop(0)
            # print(f"minute {minute} queue: {path}")
            (ore_bots, clay_bots, obsidian_bots, geode_bots,
             ore_count, clay_count, obsidian_count, geode_count) = path

            if minute == minutes - 1:
                new_path = (ore_bots,
                            clay_bots,
                            obsidian_bots,
                            geode_bots,
                            ore_count + ore_bots,
                            clay_count + clay_bots,
                            obsidian_count + obsidian_bots,
                            geode_count + geode_bots
                            )
                new_queue.append(new_path)
                continue

            # check if we can build a geode bot
            if ore_count >= blueprint.geode_robot_ore_cost and obsidian_count >= blueprint.geode_robot_obsidian_cost:
                new_path = (ore_bots,
                            clay_bots,
                            obsidian_bots,
                            geode_bots + 1,
                            ore_count + ore_bots - blueprint.geode_robot_ore_cost,
                            clay_count + clay_bots,
                            obsidian_count + obsidian_bots - blueprint.geode_robot_obsidian_cost,
                            geode_count + geode_bots
                            )
                # print(f"     next path: building a geode bot: {new_path}")
                id = (ore_bots,
                      clay_bots,
                      obsidian_bots,
                      geode_bots + 1,
                      ore_count + ore_bots - blueprint.geode_robot_ore_cost,
                      clay_count + clay_bots,
                      obsidian_count + obsidian_bots - blueprint.geode_robot_obsidian_cost,
                      geode_count + geode_bots
                      )
                if id not in visited:
                    new_queue.append(new_path)
                    visited.add(id)
                continue

            # check if we can build an obsidian bot
            if obsidian_bots < obsidian_bots_required:
                if ore_count >= blueprint.obsidian_robot_ore_cost and clay_count >= blueprint.obsidian_robot_clay_cost:
                    new_path = (ore_bots,
                                clay_bots,
                                obsidian_bots + 1,
                                geode_bots,
                                ore_count + ore_bots - blueprint.obsidian_robot_ore_cost,
                                clay_count + clay_bots - blueprint.obsidian_robot_clay_cost,
                                obsidian_count + obsidian_bots,
                                geode_count + geode_bots
                                )
                    # print(f"     next path: building an obisidian bot: {new_path}")
                    id = (ore_bots,
                          clay_bots,
                          obsidian_bots + 1,
                          geode_bots,
                          ore_count + ore_bots - blueprint.obsidian_robot_ore_cost,
                          clay_count + clay_bots - blueprint.obsidian_robot_clay_cost,
                          obsidian_count + obsidian_bots,
                          geode_count + geode_bots
                          )
                    if id not in visited:
                        new_queue.append(new_path)
                        visited.add(id)

            # check if we can build a clay bot
            if clay_bots < clay_bots_required:
                if ore_count >= blueprint.clay_robot_ore_cost:
                    new_path = (ore_bots,
                                clay_bots + 1,
                                obsidian_bots,
                                geode_bots,
                                ore_count + ore_bots - blueprint.clay_robot_ore_cost,
                                clay_count + clay_bots,
                                obsidian_count + obsidian_bots,
                                geode_count + geode_bots
                                )
                    # print(f"     next path: building a clay bot: {new_path}")
                    id = (ore_bots,
                          clay_bots + 1,
                          obsidian_bots,
                          geode_bots,
                          ore_count + ore_bots - blueprint.clay_robot_ore_cost,
                          clay_count + clay_bots,
                          obsidian_count + obsidian_bots,
                          geode_count + geode_bots
                          )
                    if id not in visited:
                        new_queue.append(new_path)
                        visited.add(id)

            # check if we can build an ore bot
            if ore_bots < ore_bots_required:
                if ore_count >= blueprint.ore_robot_ore_cost:
                    new_path = (ore_bots + 1,
                                clay_bots,
                                obsidian_bots,
                                geode_bots,
                                ore_count + ore_bots - blueprint.ore_robot_ore_cost,
                                clay_count + clay_bots,
                                obsidian_count + obsidian_bots,
                                geode_count + geode_bots
                                )
                    # print(f"     next path: building an ore bot: {new_path}")
                    id = (ore_bots + 1,
                          clay_bots,
                          obsidian_bots,
                          geode_bots,
                          ore_count + ore_bots - blueprint.ore_robot_ore_cost,
                          clay_count + clay_bots,
                          obsidian_count + obsidian_bots,
                          geode_count + geode_bots
                          )
                    if id not in visited:
                        new_queue.append(new_path)
                        visited.add(id)

            # do nothing, just gather minerals
            if True:
                new_path = (ore_bots,
                            clay_bots,
                            obsidian_bots,
                            geode_bots,
                            ore_count + ore_bots,
                            clay_count + clay_bots,
                            obsidian_count + obsidian_bots,
                            geode_count + geode_bots
                            )
                # print(f"     next path: just gathering minerals: {new_path}")
                id = (ore_bots,
                      clay_bots,
                      obsidian_bots,
                      geode_bots,
                      ore_count + ore_bots,
                      clay_count + clay_bots,
                      obsidian_count + obsidian_bots,
                      geode_count + geode_bots
                      )
                if id not in visited:
                    new_queue.append(new_path)
                    visited.add(id)

        queue = new_queue

    return max([q[7] for q in queue])


def result(input, minutes):
    blueprints = [Blueprint.from_line(line) for line in input]

    return sum([bp.number * bfs(bp, minutes) for bp in blueprints])


"""
tijdens minute 12 wordt een new path aanzien als reeds visited 
door een new path van een ander path 

12
            queue: (11, 1, 5, 0, 0, 1, 20, 0, 0)
                next path: just gather minerals (12, 1, 5, 0, 0, 2, 25, 0, 0)
            queue: (11, 1, 4, 0, 0, 3, 20, 0, 0)
                next path: building an obsidian bot (12, 1, 4, 1, 0, 1, 10, 0, 0)
                next path: building a clay bot (12, 1, 5, 0, 0, 2, 24, 0, 0)
                next path: just gather minerals (12, 1, 4, 0, 0, 4, 24, 0, 0)
queue: (11, 1, 3, 1, 0, 2, 4, 0, 0)
>>     next path: building a clay bot (12, 1, 4, 1, 0, 1, 7, 1, 0)
     next path: just gather minerals (12, 1, 3, 1, 0, 3, 7, 1, 0)
            queue: (11, 2, 3, 0, 0, 1, 18, 0, 0)
                next path: just gather minerals (12, 2, 3, 0, 0, 3, 21, 0, 0)
            queue: (11, 1, 3, 0, 0, 5, 18, 0, 0)

"""
