# Advent of Code - Day 4 - Part One

def result(input):
    pairs = 0
    for line in input:
        assignments = line.split(',')
        assignments = [list(map(int, assignment.split('-'))) for assignment in assignments]

        first = set(range(assignments[0][0], assignments[0][1]+1))
        second = set(range(assignments[1][0], assignments[1][1]+1))

        if first.issubset(second) or second.issubset(first):
            pairs += 1

    return pairs
