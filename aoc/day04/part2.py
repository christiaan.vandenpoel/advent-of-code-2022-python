# Advent of Code - Day 4 - Part Two

def result(input):
    pairs = 0
    for line in input:
        assignments = line.split(',')
        assignments = [list(map(int, assignment.split('-'))) for assignment in assignments]

        first = set(range(assignments[0][0], assignments[0][1]+1))
        second = set(range(assignments[1][0], assignments[1][1]+1))

        if len(first & second) > 0:
            pairs += 1

    return pairs
