# Advent of Code - Day 3 - Part One
from aoc.utils import array

def intersection(first, second):
    result = [value for value in first if value in second]
    return result

def result(input):
    result = 0
    for line in input:
        chunks = array.split_in_chunks(line, int(len(line)/2))
        intersect = intersection(chunks[0], chunks[1])

        first = ord(intersect[0])
        if first >= ord('a') and first <= ord('z'):
            result += first - ord('a') + 1
        else:
            result += first - ord('A') + 27

    return result
