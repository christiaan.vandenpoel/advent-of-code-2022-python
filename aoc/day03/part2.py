# Advent of Code - Day 3 - Part Two
from aoc.utils import array

def result(input):
    n = 3
    result = 0
    chunks = array.split_in_chunks(input, n)

    for chunk in chunks:
        intersect = [value for value in chunk[0] if value in chunk[1] and value in chunk[2]]
        first = ord(intersect[0])
        if first >= ord('a') and first <= ord('z'):
            result += first - ord('a') + 1
        else:
            result += first - ord('A') + 27
    return result
