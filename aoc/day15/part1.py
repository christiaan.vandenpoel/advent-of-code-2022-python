# Advent of Code - Day 15 - Part One
import re

def distance(first, second):
    return abs(first[0]-second[0]) + abs(first[1]-second[1])

def result(input, y):
    sensors_and_beacons = []
    for line in input:
        line = line.replace("Sensor at ","").replace(": closest beacon is at ",";")
        m = re.search('x=(?P<sensor_x>-?\\d+), y=(?P<sensor_y>-?\\d+);x=(?P<beacon_x>-?\\d+), y=(?P<beacon_y>-?\\d+)', line).groups()
        sensor = (int(m[0]), int(m[1]))
        beacon = (int(m[2]), int(m[3]))
        sensors_and_beacons.append({'sensor': sensor, 'beacon': beacon, 'distance': distance(sensor, beacon)})

    sensors_covering_row = []
    for sensor_and_beacon in sensors_and_beacons:
        destination_to_row = (sensor_and_beacon['sensor'][0], y)
        distance_to_row = distance(destination_to_row, sensor_and_beacon['sensor'])
        if distance_to_row <= sensor_and_beacon['distance']:
            sensors_covering_row.append(sensor_and_beacon)

    no_beacons = set()
    for sensor_and_beacon in sensors_covering_row:
        destination_to_row = (sensor_and_beacon['sensor'][0], y)
        no_beacons.add(destination_to_row)
        x = sensor_and_beacon['sensor'][0] - 1
        while distance((x,y), sensor_and_beacon['sensor']) <= sensor_and_beacon['distance']:
            no_beacons.add((x,y))
            x -= 1
        x = sensor_and_beacon['sensor'][0] + 1
        while distance((x,y), sensor_and_beacon['sensor']) <= sensor_and_beacon['distance']:
            no_beacons.add((x,y))
            x += 1

    for sensor_and_beacon in sensors_and_beacons:
        beacon = sensor_and_beacon['beacon']
        if beacon in no_beacons:
            no_beacons.remove(beacon)

    return len(no_beacons)
