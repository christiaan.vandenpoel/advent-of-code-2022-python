# Advent of Code - Day 15 - Part Two
import re

def distance(first, second):
    return abs(first[0]-second[0]) + abs(first[1]-second[1])

def result(input, maximum):
    sensors_and_beacons = []
    for line in input:
        line = line.replace("Sensor at ","").replace(": closest beacon is at ",";")
        m = re.search('x=(?P<sensor_x>-?\\d+), y=(?P<sensor_y>-?\\d+);x=(?P<beacon_x>-?\\d+), y=(?P<beacon_y>-?\\d+)', line).groups()
        x_sensor, y_sensor = (int(m[0]), int(m[1]))
        sensor = (x_sensor, y_sensor)
        beacon = (int(m[2]), int(m[3]))
        dist = distance(sensor, beacon)
        obj = {'sensor': sensor, 'beacon': beacon, 'distance': dist}

        coordinates = set()
        # left most point just outside of distance
        left = (x_sensor - dist - 1, y_sensor)
        assert distance(left, sensor) - dist == 1
        dy = 1
        y_up = y_sensor
        y_down = y_sensor
        if left > (0,0):
            coordinates.add(left) 
        for x in range(x_sensor - dist, x_sensor + dist + 2):
            y_up += dy
            y_down -= dy
            if x < 0 or x >= maximum:
                continue

            if y_up >= 0 and y_up < maximum:
                assert distance((x, y_up), sensor) - dist == 1
                coordinates.add((x, y_up))
            if y_down >= 0 and y_down < maximum:
                assert distance((x, y_down), sensor) - dist == 1
                coordinates.add((x, y_down))
            if x == x_sensor:
                dy = -1

        obj['surroundings'] = coordinates
        sensors_and_beacons.append(obj)

    for sensor in sensors_and_beacons:
        others = [sab for sab in sensors_and_beacons if sab != sensor]
        for surrounding in sensor['surroundings']:
            outside = True
            for other in others:
                if distance(surrounding, other['sensor']) <= other['distance']:
                    outside &= False
            if outside:
                return surrounding[0] * 4_000_000 + surrounding[1]