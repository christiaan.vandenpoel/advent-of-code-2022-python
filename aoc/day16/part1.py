# Advent of Code - Day 16 - Part One
from .valve import Graph

def result(input):
    return Graph.from_text("\n".join(input)).optimise_pressure_relief()
