# Advent of Code - Day 16 - Part Two
from .valve import Graph, double_up


def result(input):
    graph = Graph.from_text("\n".join(input))
    return double_up(graph)
