from typing import Tuple


class Node():
    def __init__(self, value) -> None:
        self.value = value
        self.previous = None
        self.next = None

    def __repr__(self) -> str:
        return f"Node(val:{self.value} prev:{self.previous.value} next:{self.next.value})"


def parse(input, decryption_key=1) -> Tuple[list[Node], Node]:
    nodes = []

    zero = None
    for line in input:
        nodes.append(node := Node(int(line)*decryption_key))
        if node.value == 0:
            zero = node
    length = len(nodes)
    for idx, node in enumerate(nodes):
        node.previous = nodes[(idx-1+length) % length]
        node.next = nodes[(idx+1) % length]

    return nodes, zero


def mix(nodes: list[Node]):
    for node in nodes:
        node.previous.next = node.next
        node.next.previous = node.previous

        delta = node.value % (len(nodes) - 1)
        curr = node.previous
        while delta > 0:
            curr = curr.next
            delta -= 1
        while delta < 0:
            curr = curr.previous
            delta += 1

        node.previous = curr
        node.next = curr.next
        curr.next.previous = node
        curr.next = node


def sum(zero):
    sum = 0
    curr = zero
    for x in range(3001):
        if x in [1000, 2000]:
            sum += curr.value
        if x == 3000:
            return sum + curr.value
        curr = curr.next
