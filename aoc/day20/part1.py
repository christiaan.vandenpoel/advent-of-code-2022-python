# Advent of Code - Day 20 - Part One
from .grove_positioning import parse, mix, sum


def result(input):
    nodes, zero = parse(input)
    mix(nodes)
    return sum(zero)
