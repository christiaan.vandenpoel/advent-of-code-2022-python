# Advent of Code - Day 20 - Part Two
from .grove_positioning import parse, mix, sum


def result(input):
    nodes, zero = parse(input, decryption_key=811589153)
    for _ in range(10):
        mix(nodes)
    return sum(zero)
