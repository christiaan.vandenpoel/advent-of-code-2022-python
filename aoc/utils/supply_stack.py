from aoc.utils import array

def parse_stack_and_movements(input):
    state = dict()
    start, movements = "\n".join(input).split("\n\n")[0:2]
    start = list(reversed(start.splitlines()))
    movements = movements.splitlines()
    stacks = map(int, array.split_in_chunks(start[0], 4))
    for i in stacks:
        state[i] = []

    for line in start[1:]:
        n = 4
        for col in range(0, len(line), n):
            container = line[col:col+n].replace("[", "").replace("]","").replace(" ", "")
            if container != '':
                state[int(col/4 + 1)].append(container)
    
    return state, movements

def move_one_by_one(state, movements):
    for movement in movements:
        _, count, _, src, _, dst = movement.split(" ")
        count = int(count)
        src = int(src)
        dst = int(dst)
        
        for _ in range(0, count):
            state[dst].append(state[src].pop())

    return "".join([value[-1] for value in state.values()])

def move_all_at_once(state, movements):
    for movement in movements:
        _, count, _, src, _, dst = movement.split(" ")
        count = int(count)
        src = int(src)
        dst = int(dst)
        
        temp = []
        for _ in range(0, count):
            temp.append(state[src].pop())
        
        for item in reversed(temp):
            state[dst].append(item)

    return "".join([value[-1] for value in state.values()])    