# Advent of Code - Day 8 - Part Two

def result(input):
    tree_map = dict()
    for y, line in enumerate(input):
        for x, char in enumerate(line):
            tree_map[(x,y)] = int(char)
    height = y + 1
    width = x + 1

    scenic_scores = set()

    for row in range(0, height):
        for col in range(0, width):
            start_loc = (col, row)
            start_value = tree_map[start_loc]
            accumulator = 1
            # go left
            temp = 0
            for x in reversed(range(0, col)):
                temp += 1
                value_at_loc = tree_map[(x, row)]
                if value_at_loc >= start_value:
                    break
            accumulator *= temp

            # go right
            temp = 0
            for x in range(col+1, width):
                temp += 1
                value_at_loc = tree_map[(x, row)]
                if value_at_loc >= start_value:
                    break
            accumulator *= temp

            # go up
            temp = 0
            for y in reversed(range(0, row)):
                temp += 1
                value_at_loc = tree_map[(col, y)]
                if value_at_loc >= start_value:
                    break
            accumulator *= temp

            # go down
            temp = 0
            for y in range(row+1, height):
                temp += 1
                value_at_loc = tree_map[(col, y)]
                if value_at_loc >= start_value:
                    break
            accumulator *= temp
            scenic_scores.add(accumulator)

    return sorted(scenic_scores, reverse=True)[0]
