# Advent of Code - Day 8 - Part One

def result(input):
    tree_map = dict()
    for y, line in enumerate(input):
        for x, char in enumerate(line):
            tree_map[(x,y)] = int(char)
    height = y + 1
    width = x + 1

    visible = set()
    # from the left
    for row in range(1, height - 1):
        last_value = tree_map[(0,row)]
        for col in range(1,width - 1):
            tree_height = tree_map[(col, row)]
            if tree_height > last_value:
                visible.add((col, row))
                last_value = max(tree_height, last_value)

    # from the top
    for col in range(1, width - 1):
        last_value = tree_map[(col,0)]
        for row in range(1, height-1):
            tree_height = tree_map[(col, row)]
            if tree_height > last_value:
                visible.add((col, row))
                last_value = max(tree_height, last_value)

    # # from the right
    for row in range(1, height - 1):
        last_value = tree_map[(width - 1,row)]
        for col in range(width - 2, 0, -1):
            tree_height = tree_map[(col, row)]
            if tree_height > last_value:
                visible.add((col, row))
                last_value = max(tree_height, last_value)

    # from the bottom
    for col in range(1, width - 1):
        last_value = tree_map[(col, height-1)]
        for row in range(height - 2, 0, -1):
            tree_height = tree_map[(col, row)]
            if tree_height > last_value:
                visible.add((col, row))
                last_value = max(tree_height, last_value)

    return len(visible) + 2 * height + 2 * (width - 2)
