def solve(line, size):
    for idx in range(0, len(line)):
        last = line[0:idx+1][-size:]
        if len(set(last)) == size:
            return idx + 1