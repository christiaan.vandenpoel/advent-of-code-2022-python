# Advent of Code - Day 6 - Part Two
from aoc.day06.util import solve

def result(input):
    line = input[0]
    return solve(line, 14)
