# Advent of Code - Day 6 - Part One
from aoc.day06.util import solve

def result(input):
    line = input[0]
    return solve(line, 4)
