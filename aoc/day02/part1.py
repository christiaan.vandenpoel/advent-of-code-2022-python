# Advent of Code - Day 2 - Part One

opponent = {
    'A': 'Rock',
    'B': 'Paper',
    'C': 'Scissors'
}

you = {
    'X': 'Rock',
    'Y': 'Paper',
    'Z': 'Scissors'
}

scoring = {
    ('Rock','Rock'): 1 + 3,
    ('Rock','Paper'): 2 + 6,
    ('Rock','Scissors'): 3 + 0,
    ('Paper','Rock'): 1 + 0,
    ('Paper','Paper'): 2 + 3,
    ('Paper','Scissors'): 3 + 6,
    ('Scissors','Rock'): 1 + 6,
    ('Scissors','Paper'): 2 + 0,
    ('Scissors','Scissors'): 3 + 3,
}

def result(input):
    total = 0
    for game in input:
        total += scoring[(opponent[game[0]],you[game[2]])]
    return total
