# Advent of Code - Day 2 - Part Two

opponent = {
    'A': 'Rock',
    'B': 'Paper',
    'C': 'Scissors'
}

you = {
    'X': 'Rock',
    'Y': 'Paper',
    'Z': 'Scissors'
}

scoring = {
    'A X': 3 + 0,
    'B X': 1 + 0,
    'C X': 2 + 0,
    'A Y': 1 + 3,
    'B Y': 2 + 3,
    'C Y': 3 + 3,
    'A Z': 2 + 6,
    'B Z': 3 + 6,
    'C Z': 1 + 6
}

def result(input):
    total = 0
    for game in input:
        total += scoring[game]
    return total