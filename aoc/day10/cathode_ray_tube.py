
def process(instructions):
    accumulator = 0
    x = 1
    cycle = 0
    crt = ""
    for instruction in instructions:
        parts = instruction.split(" ")

        if parts[0] == 'noop':
            ticks_to_wait = 1
            value = None
        elif parts[0] == 'addx':
            ticks_to_wait = 2;
            value = int(parts[1])

        while ticks_to_wait > 0:
            cycle += 1

            offset = (cycle - 1) % 40
            if offset == x - 1 or offset == x or offset == x + 1:
                crt += "🎁"
            else:
                crt += "🎄"

            if cycle % 40 == 0:
                crt += "\n"

            if (cycle - 20) % 40 == 0:
                accumulator += cycle * x
        
            ticks_to_wait -= 1

        if value is not None:
            x += value
    
    return accumulator, "\n" + crt