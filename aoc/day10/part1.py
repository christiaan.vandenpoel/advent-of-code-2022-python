# Advent of Code - Day 10 - Part One
from aoc.day10.cathode_ray_tube import process

def result(input):
    acc, _ = process(input)
    return acc
