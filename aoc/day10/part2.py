# Advent of Code - Day 10 - Part Two
from aoc.day10.cathode_ray_tube import process

def result(input):
    _, crt = process(input)
    return crt